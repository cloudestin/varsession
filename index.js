function set( variable , value ) {
	 console.info("*** Deprecated function set. Replaced by session(name, value);");
	 if (this.session) this.session[variable] = value;
	}
function get( variable ) {
	 console.error("*** Deprecated function get. Replaced by session(\""+variable+"\");");
	 if (!this.session) return null;
	 return this.session[variable];
	}
function clear( variable ) {
	 console.info("*** Deprecated function clear. Replaced by session(name,null);");
	 if (this.session) delete this.session[variable];
	}
function setAttr( variable, field, value ) {
	 console.info("*** Deprecated function setAttr. Replaced by session(name,field, value);");
	 if (this.session && this.session[variable]) this.session[variable][field] = value;
	}
function ip( ) {
	 return this.ip;
	}
function abandon( ) {
	 if (this.session) this.session.destroy();
	}
function main( variable, field, value ) {
	var result = null;
	 if (!this.session) return null; // no sessions available

	 switch(arguments.length){
	 	case 0 : result = this.session; break; 
	 	case 1 : result = this.session[variable]; break;
	 	case 2 : if (typeof field == "string") result = (this.session[ variable ] || {})[ field ];
	 				else if (field === null) delete this.session[variable]
			 		else this.session[variable] = value;
			 		break;
	 	case 3 : if (!this.session[variable]) this.session[variable] = {};
			 		this.session[variable][field] = value;
			 		break;
	 }
	 return result;
	}

function setAttr( variable, field, value ) {
	 if (this.session && this.session[variable]) this.session[variable][field] = value;
	}

module.exports = function( req ){
	 var exports = main.bind(req);
	 exports.ip = ip.bind(req);
	 exports.abandon = abandon.bind(req);

	 // deprecated functions
	 exports.set = set.bind(req);
	 exports.get = get.bind(req);
	 exports.setAttr = setAttr.bind(req);
	 exports.clear = clear.bind(req);
	 // deprecated functions

	 return exports;
	}
